### A dockerised micro-service node.js architecture

This repo contains the sourc for the following posts.

- [The Internet of things, Dockerising RabbitMQ and MQTT](http://blog.airasoul.io/dockerising-rabbitmq-and-mqtt/)
- [The Internet of things, with RabbitMQ, Node.js, MQTT and AMQP](http://blog.airasoul.io/the-internet-of-things-with-rabbitmq-node-js-mqtt-and-amqp/)
